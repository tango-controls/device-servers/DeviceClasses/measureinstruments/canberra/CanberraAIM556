//=============================================================================
//
// file :        AIM556_def.h
//
// description : Include file for the AIM556 definitions
//
// project :     MCA Canberra AIM 556
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
//=============================================================================
#ifndef  AIM556_DEFH
#define  AIM556_DEFH

/* Pack all structures on 1-byte boundaries */
#pragma pack(1)

typedef char int8;
typedef unsigned char uint8;

typedef short int16;
typedef unsigned short uint16;

typedef int int32;
typedef unsigned int uint32;

#define NCP_K_HCMD_SETACQADDR   1               /* SET ACQUISITION ADDRESS */
struct ncp_hcmd_setacqaddr {
        uint16 adc;                             /* ADC number */
        uint32 address;                         /* Acquisition address */
        uint32 limit;                           /* Acquisition limit address */
};

#define NCP_K_HCMD_SETELAPSED   2               /* SET ELAPSED TIMES */
struct ncp_hcmd_setelapsed {
        uint16 adc;
        uint32 live;                            /* Elapsed live time */
        uint32 real;                            /*         real      */
};

#define NCP_K_HCMD_SETMEMORY    3               /* SET MEMORY */
struct ncp_hcmd_setmemory {
        uint32 address;                         /* Destination address of data */
        uint32 size;                            /* Amount of data (bytes) */
                                                /* Start of data */
};

#define NCP_K_HCMD_SETMEMCMP    4               /* SET MEMORY COMPRESSED */
struct ncp_hcmd_setmemcmp {
        uint32 address;                         /* Destination address of data */
        uint32 size;                            /* Amount of data (bytes) */
                                                /* Start of data */
};

#define NCP_K_HCMD_SETPRESETS   5               /* SET PRESETS */
struct ncp_hcmd_setpresets {
        uint16 adc;
        uint32 live;                            /* Preset live time */
        uint32 real;                            /*        real      */
        uint32 totals;                          /* Preset total counts */
        uint32 start;                    	/* Preset totals start channel */
        uint32 end;                      	/*               end channel */
        uint32 limit;                    	/* Preset limit */
};

#define NCP_K_HCMD_SETACQSTATUS 6               /* SET ACQUISITION STATUS */
struct ncp_hcmd_setacqstate {
        uint16 adc;
        int8 status;                            /* New acquisition status (on/off) */
};

#define NCP_K_HCMD_ERASEMEM     7               /* ERASE MEMORY */
struct ncp_hcmd_erasemem {
        uint32 address;                  	/* Start address of memory to be erased */
        uint32 size;                     	/* Number of bytes to be erased */
};

#define NCP_K_HCMD_SETACQMODE   8               /* SET ACQUISITION MODE */
struct ncp_hcmd_setacqmode {
        uint16 adc;
        int8 mode;                              /* Acquisition mode; see NCP_C_AMODE_xxx */
};

#define NCP_K_HCMD_RETMEMORY    9               /* RETURN MEMORY */
struct ncp_hcmd_retmemory {
        uint32 address;                  	/* Start address of memory to return */
        uint32 size;                     	/* Number of bytes of memory to return */
};

#define NCP_K_HCMD_RETMEMCMP    10              /* RETURN MEMORY COMPRESSED */
struct ncp_hcmd_retmemcmp {
        uint32 address;                  	/* Start address of memory to return */
        uint32 size;                     	/* Number of bytes of memory to return */
};
#define NCP_K_MRESP_RETMEMCMP   227
struct ncp_mresp_retmemcmp {
        uint32 channels;                 	/* Number of channels of encoded data following */
                                                /* Encoded data starts here */
};

#define NCP_K_HCMD_RETADCSTATUS 11              /* RETURN ADC STATUS */
struct ncp_hcmd_retadcstatus {
        uint16 adc;
};
#define NCP_K_MRESP_ADCSTATUS   35
struct ncp_mresp_retadcstatus {
        int8 status;                            /* ADC on/off status */
        uint32 live;                     	/* Elapsed live time */
        uint32 real;                     	/*         real      */
        uint32 totals;                   	/*         total counts */
};

#define NCP_K_HCMD_SETHOSTMEM   13              /* SET HOST MEMORY */
struct ncp_hcmd_sethostmem {
        uint32 address;                  	/* Destination address of host data */
        uint32 size;                     	/* Amount of data (bytes) */
                                                /* Start of data */
};

#define NCP_K_HCMD_RETHOSTMEM   14              /* RETURN HOST MEMORY */
struct ncp_hcmd_rethostmem {
        uint32 address;                  	/* Start address of host memory to return */
        uint32 size;                     	/* Number of bytes of memory to return */
};

#define NCP_K_HCMD_SETOWNER     15              /* SET OWNER */
struct ncp_hcmd_setowner {
        uint8 owner_id[6];              	/* New owner ID */
        int8 owner_name[8];                     /* New owner name */
};

#define NCP_K_HCMD_SETOWNEROVER 16              /* SET OWNER OVERRIDE */
struct ncp_hcmd_setownerover {
        uint8 owner_id[6];              	/* New owner ID */
        int8 owner_name[8];                     /* New owner name */
};

#define NCP_K_HCMD_RESET        17              /* RESET MODULE */
#define NCP_K_HCMD_SETDISPLAY   18              /* SET DISPLAY CHARACTERISTICS */
#define NCP_K_HCMD_RETDISPLAY   19              /* RETURN DISPLAY */
#define NCP_K_HCMD_DIAGNOSE     20              /* DIAGNOSE */

#define NCP_K_HCMD_SENDICB      21              /* SEND TO ICB */
struct ncp_hcmd_sendicb {
        uint32 registers;                	/* Number of address/data pairs to write */
        struct {
           uint8 address;               	/* ICB address where data will be sent */
           uint8 data;                  	/* data to write */
        }  addresses[64];
};

#define NCP_K_HCMD_RECVICB      22              /* RECEIVE FROM ICB */
struct ncp_hcmd_recvicb {
        uint32 registers;                	/* Number of addresses to read */
        uint8 address[64];              	/* ICB addresses from which data is to be read */
};

#define NCP_K_HCMD_SETUPACQ     23              /* SETUP ACQUISITION */
struct ncp_hcmd_setupacq {
        uint16 adc;
        uint32 address;                  	/* Acquisition address */
        uint32 alimit;                   	/* Acquisition limit address */
        uint32 plive;                    	/* Preset live time */
        uint32 preal;                    	/*        real      */
        uint32 ptotals;                  	/* Preset total counts */
        uint32 start;                    	/* Preset totals start channel */
        uint32 end;                      	/*               end channel */
        uint32 plimit;                   	/* Preset limit */
        uint32 elive;                    	/* Elapsed live time */
        uint32 ereal;                    	/*         real      */
        int8 mode;                              /* Acquisition mode; see NCP_C_AMODE_xxx */
};

#define NCP_K_HCMD_RETACQSETUP  24              /* RETURN ACQUISITION SETUP INFO */
struct ncp_hcmd_retacqsetup {
        uint16 adc;
};
#define NCP_K_MRESP_RETACQSETUP 203
struct ncp_mresp_retacqsetup {
        uint32 address;                  	/* Acquisition address */
        uint32 alimit;                   	/* Acquisition limit address */
        uint32 plive;                    	/* Preset live time */
        uint32 preal;                    	/*        real      */
        uint32 ptotals;                  	/* Preset total counts */
        uint32 start;                    	/* Preset totals start channel */
        uint32 end;                      	/*               end channel */
        uint32 plimit;                   	/* Preset limit */
        int8 mode;
};

#define NCP_K_HCMD_SETMODEVSAP  25              /* SET MODULE EVENT MESSAGE ADDRESSES*/
struct ncp_hcmd_setmodevsap {
        uint16  mevsource;              	/* 0-255 are reserved for    */
                                                /* adc numbers. 255-65536    */
                                                /* are types NCP_K_MEVSRC_   */
        int8    mev_dsap;                       /* dest. service access point */
        int8    mev_ssap;                       /* sour. service access point */
        int8    snap_id[5];                     /* SNAP protocol ID */
};

#define NCP_K_HCMD_RETLISTMEM   26              /* RETURN LIST BUFFER */
struct ncp_hcmd_retlistmem {
        uint16 adc;                     	/* adc number */
        uint32 offset;                   	/* byte offset from start of buffer of transfer start */
        uint32 size;                     	/* number of bytes to return */
        int8 buffer;                            /* 0 for "1st" buffer, 1 for "2nd" */
};

#define NCP_K_HCMD_RELLISTMEM   27              /* RELEASE LIST BUFFER */
struct ncp_hcmd_rellistmem {
        uint16 adc;                     	/* adc number */
        int8 buffer;                            /* 0 for "1st" buffer, 1 for "2nd" */
};

#define NCP_K_HCMD_RETLISTSTAT  28              /* RETURN LIST ACQ STATUS */
struct ncp_hcmd_retliststat {
        uint16 adc;                     	/* adc number */
};

#define NCP_K_MRESP_RETLISTSTAT 251
struct ncp_mresp_retliststat {
        int8 status;                            /* acquire on/off state */
        int8 current_buffer;                    /* current acquire buffer: 0 for "1st" buffer, 1 for "2nd" */
        int8 buffer_1_full;                     /* 1st buffer status: 1 if full of data */
        uint32 offset_1;                 	/* number of bytes of data in 1st buffer */
        int8 buffer_2_full;                     /* 2nd buffer status: 1 if full of data */
        uint32 offset_2;                 	/* number of bytes of data in 2nd buffer */
};

#define NCP_K_HCMD_RETMEMSEP    29              /* RETURN MEMORY SEPARATED */
struct ncp_hcmd_retmemsep {
        uint32 address;                  	/* start address of first chunk of memory to return*/
        uint32 size;                     	/* number of bytes per chunk */
        uint32 offset;                   	/* byte offset between start of each chunk */
        uint32 chunks;                   	/* number of chunks to return */
};                     /* Note: total bytes returned will be */
                                                /*      size*chunks */

#define NCP_K_HCMD_RESETLIST    30              /* RESET LIST MODE */
struct ncp_hcmd_resetlist {
        uint16 adc;                     	/* adc number */
};

/* 
 * Subnetwork access protocol (SNAP)
 */

#define LLC_SAP_SNAPLLC 0x02
#define LLC_SAP_SNAPEXT 0xAA
#define SNAP_SIZE    5

/*
* Define format for host memory usage. This is used by hosts to store module
* setup information in the module itself. There are 256 bytes of memory total.
*/

        struct nam_hostmem_v0_struct {

           int8 initialized;                    /* True if host mem has been setup */
           int8 version;                        /* Version number of this structure */
           int32 acqmem_usage_map[4];           /* Map of 8K blocks of acq memory */

           struct {
                int8 used;                      /* This input is used */
                int8 name[29];                  /* Configuration name of the input */
                int8 owner[12];                 /* Name of input's owner */
                int32 channels;                 /* Number of channels assigned to input */
                uint16 rows;            	/*           rows */
                uint8 groups;           	/*           groups */
                int32 memory_start;             /* Start of acq mem for this input */
                uint8 year;             	/* Acquisition start year (base = 1900) */
                uint8 month;            	/*                   month */
                uint8 day;              	/*                   day */
                uint8 hour;             	/*                   hour */
                uint8 minute;           	/*                   minute */
                uint8 seconds;          	/*                   seconds */
           } input[4];
        };

/*
* This structure defines the return argument from NMC_ACQU_GETACQSETUP; it
* characterizes the acquisition setup state of an AIM ADC.
*/

struct nmc_acqsetup_struct {

        int8 name[33];                          /* configuration name */
        int8 owner[12];                         /*               owner */
        int32 channels;
        int32 rows;
        int32 groups;
        int32 memory_start;                     /* acq memory start address */
        int32 plive;                            /* preset live time */
        int32 preal;                            /*        real      */
        int32 ptotals;                          /*        total counts */
        int32 start;                            /* preset totals start region */
        int32 end;                              /* preset totals end region */
        int32 plimit;                           /* preset limit */
        int32 mode;                             /* acquisition mode */
        int32 astime[2];                        /* acqusition start date/time */
        int32 agroup;                           /* acquire group */
        int32 status;                           /* acquire status */
        int32 live;                             /* elapsed live time */
        int32 real;                             /*         real      */
        int32 totals;                           /*         total counts */
};

/*
* Miscellaneous definitions
*/

/*
* Module acquisition modes
*/

#define NCP_C_AMODE_PHA         1               /* PHA */
#define NCP_C_AMODE_MCS         2               /* Multi Channel Scanner */
#define NCP_C_AMODE_DLIST       3               /* Double buffered list */

/*
* Define structure of protocol header block. Fields marked with a "###" are not
* used in Ethernet messages.
*/

struct ncp_comm_header {

        int32 checkword;                        /* Unique number pattern (NCP_K_CHECKWORD) */
        int8 protocol_type;                     /* Protocol type */
        uint8 protocol_flags;                   /* Protocol flags */
        uint8 message_number;                   /* Message number */
        int8 message_type;                      /* Message type */
        uint8 owner_id[6];                      /* ID of owner of module */
        int8 owner_name[8];                     /* Name of owner of module */
        uint32 data_size;                       /* Size of message data area */
        uint8 module_id;                        /* Module ID number ### */
        uint8 submessage_number;                /* Sequences multi-block commands */
        int8 spares[2];                         /* Spares */
        uint16 checksum;                        /* Header checksum ### */
};

/*
* Define protocol_type. This field identifies the type/rev of the communications
* protocol in use.
*/

#define NCP_C_PRTYPE_NAM        1               /* Standard NAM-Host protocol */

/*
* Define protocol_flags. This field contains bit flags for the use of the
* communications handlers.
*/

        /* No flags defined yet */

/*
* Define message_type. This field defines the basic format of the data area.
*/

#define NCP_C_MSGTYPE_PACKET    1               /* Command/Response packets */
#define NCP_C_MSGTYPE_MSTATUS   2               /* Module status */
#define NCP_C_MSGTYPE_MEVENT    3               /* Module event message */
#define NCP_C_MSGTYPE_INQUIRY   4               /* Host inquiry */

/*
* Define data area packet format.
*/

struct ncp_comm_packet {

        uint32 packet_size;                     /* Packet data area size */
        int8 packet_type;                       /* Packet type (command/response) */
        uint8 packet_flags;                     /* Packet flags */
        int16 packet_code;                      /* Packet command/response code */
                                                /* Data starts here! */

};

/*
* Define packet_type. This field makes sure that commands are not interpreted as
* responses and vice versa.
*/

#define NCP_C_PTYPE_HCOMMAND    1               /* Host to module command */
#define NCP_C_PTYPE_MRESPONSE   2               /* Module to host response */

/*
* Module status message structure. This message is returned by a module in
* response to a multicast inquiry message.
* The structure below appears in the data area of the message.
*/

struct ncp_comm_mstatus {

        uint8 module_type;                      /* Module type ID */
        uint8 hw_revision;                      /* Module HW revision number */
        uint8 fw_revision;                      /* Module FW revision number */
        uint8 module_init;                      /* Module state: true if ever
                                                   has been initialized. */
        int32 comm_flags;                       /* Communications capability flags */

                                                /* Module type specific info */
                                                /* starts here! */

                /* NAM (AIM) */

        uint8 num_inputs;                       /* Number of inputs (ADCs) */
        int32 acq_memory;                       /* Amount of acquisition memory
                                                   in bytes */
        int32 spares[4];                        /* spares */
};

/* Define contents of ncp_comm_status.module_type */

#define NCP_C_MODTYPE_NAM       1               /* Network Acquisition Module */
                                                /* (We call it AIM nowadays) */

/*
* Define structure of data area for host inquiry messages
*/

struct ncp_comm_inquiry {

        int8 inquiry_type;                      /* Type of inquiry message */

};

/*
* Define values for ncp_comm_inquiry.inquiry_type
*/

#define NCP_C_INQTYPE_ALL 1                     /* Everybody responds */
#define NCP_C_INQTYPE_UNOWNED 2                 /* Unowned modules only */
#define NCP_C_INQTYPE_NOTMINE 3                 /* Modules unowned by this host only */

/*
* Module event message structure. This message is sent when an event occurs in
* the module. The structure below describes the data field of the message.
*/

struct ncp_comm_mevent {

        uint8 event_type;                       /* Event type */
        int32 event_id1;                        /* Event identifiers (type dependent) */
        int32 event_id2;
};

/* Define contents of ncp_comm_mevent.event_type */

#define NCP_C_EVTYPE_ACQOFF     1               /* Acquisition stopped:
                                                   event_id1 is the ADC number */
#define NCP_C_EVTYPE_BUFFER     2               /* List buffer filled:
                                                   event_id1 is the ADC number
                                                   event_id2 is the buffer number (0 or 1) */
#define NCP_C_EVTYPE_DIAGCOMP   3               /* Diagnostics complete */

#define NCP_C_EVTYPE_SERVICERQST 4              /* Service request aserted
                                                   on ICB Bus */

/* Define contents of ncp_hcmd_setmodevsap.mevsource */

/* 0 - 255 are recerved for ADC numbers */

#define NCP_K_MEVSRC_ICB  256                   /* Service Request from ICB */


/*
* Misc communications structures and constants
*/

#define NMC_K_MAX_NIMSG 1492                    /* Max size of an NI message */

#define NMC_K_MAX_UNANSMSG 3                    /* Max number of unanswered
                                                   inquiry messages allowed
                                                   before a module is marked
                                                   UNREACHABLE */
#define NCP_K_CHECKWORD         0XAF0366F2      /* Value of
                                                   ncp_comm_header.checkword */

/* Define the structure of an IEEE 802 extended packet header */
struct enet_header {
        uint8 dest[6];
        uint8 source[6];
        uint16 length;
};

struct snap_header {
        uint8 dsap;
        uint8 ssap;
        uint8 control;
        uint8 snap_id[5];
};

/* Define the maximum header size for all network types (Ethernet, FDDI,etc.) */
#define NCP_MAX_HEADER_SIZE (sizeof(struct enet_header)+sizeof(struct snap_header))

/* Link layer information passed at the start of each packet passed up to
   the higher layers, also including the SNAP header sent out in all packets */
struct llinfo {
        uint8 source[6];
        uint8 snap_id[5];
};

/* Define structure of a packet to/from an AIM module */
struct enet_packet{
        struct enet_header enet_header;
        struct snap_header snap_header;
        struct ncp_comm_header ncp_comm_header;
        union {
           struct ncp_comm_mstatus ncp_comm_mstatus;
           struct ncp_comm_packet  ncp_comm_packet;
           struct ncp_comm_inquiry ncp_comm_inquiry;
           struct ncp_comm_mevent  ncp_comm_mevent;
        }  data;
};

struct status_packet{
        struct enet_header enet_header;
        struct snap_header snap_header;
        struct ncp_comm_header ncp_comm_header;
        struct ncp_comm_mstatus ncp_comm_mstatus;
};
struct response_packet{
        struct enet_header enet_header;
        struct snap_header snap_header;
        struct ncp_comm_header ncp_comm_header;
        struct ncp_comm_packet  ncp_comm_packet;
        uint8 ncp_packet_data[NMC_K_MAX_NIMSG -
                                      sizeof(struct ncp_comm_header) -
                                      sizeof(struct ncp_comm_packet)];
};
struct inquiry_packet{
        struct enet_header enet_header;
        struct snap_header snap_header;
        struct ncp_comm_header ncp_comm_header;
        struct ncp_comm_inquiry ncp_comm_inquiry;
};
struct event_packet{
        struct enet_header enet_header;
        struct snap_header snap_header;
        struct ncp_comm_header ncp_comm_header;
        struct ncp_comm_mevent ncp_comm_mevent;
};

/* Revert to previous packing */
#pragma pack()

/* Number of seconds to multicast inquiry packets to the network */
#define NCP_BROADCAST_PERIOD 20

/*
* Module status codes: these are returned in NCP_COMM_PACKET.PACKET_CODE; note
* that some codes are defined in AIM_COMM_DEFS.H along with their structure
* definitions. Any code defined here has a 0 length packet data field.
*/

#define NCP_K_MRESP_SUCCESS     9               /* Successful command completion */
#define NCP_K_MRESP_INVALADC    18              /* Bad ADC number */
#define NCP_K_MRESP_INVALCMD    26              /* Bad command */
                                                /* NCP_K_MRESP_ADCSTATUS 35 */
#define NCP_K_MRESP_OWNERNOTSET 42              /* Module owned, not overridden */
                                                /*             MODCOMMERR 50 */
                                                /*             MODERROR 58 */
                                                /*             MSGTOOBIG 66 */
                                                /*             INVMODNUM 74 */
                                                /*             UNKMODULE 82 */
                                                /*             UNOWNEDMOD 90 */
                                                /*             INVMODRESP 98 */
#define NCP_K_MRESP_UNLCHN      106             /* the number of bytes/4 is not */
#define NCP_K_MRESP_INVALACQMODE        114     /* Host sent invalid acquisition mode */
                                                /* an even number of channels */
#define NCP_K_MRESP_INVALSTACQADR       122     /* Starting address of setacqaddr
                                                   command does not start on 256
                                                   byte boundry */
#define NCP_K_MRESP_ACQRNGFRC   130             /* The acqision range contains
                                                   a fractional channel */
#define NCP_K_MRESP_INVALSTMEMADR       138     /* The specified start address does
                                                   is not on an even longword boundry */
#define NCP_K_MRESP_MEMFRC              146     /* The memory range contains a fractional
                                                   channel */
#define NCP_K_MRESP_ACQONCOMMINVAL      154     /* Command invalid while acquisition
                                                   is on */
#define NCP_K_MRESP_APUTIMEOUT          162     /* APU timed out on command */
                                                /*              READIOERR 170 */
                                                /*              INVNETYPE 178 */
                                                /*              TOOMANYMODULES 186 */
                                                /*              NOSUCHMODULE 194 */
                                                /*              RETACQSETUP 203 */
                                                /*              NONISAPS 210 */
                                                /*              MODNOTREACHABLE 218 */
                                                /*              RETMEMCMP 227 */
#define NCP_K_MRESP_RQSTMEMSIZETOOLG    234     /* requested memory size too large */
#define NCP_K_MRESP_SETHOSTMEMSIZETOOLG 242     /* size was too large in set host memory */
                                                /*              RETLISTSTAT 251 */
#define NCP_K_MRESP_INVALOFFSETRETLIST  258     /* bad offset in RETLISTMEM */
#define NCP_K_MRESP_INVALLISTBUFFER     266     /* bad list buffer specified */
#define NCP_K_MRESP_DLISTBUFFNOTFULL    274     /* dlist buffer not full */
#define NCP_K_MRESP_COMMINVLINCURRMODE  282     /* Command invalid in current mode */
                                                /*              NOTOWNED 290 */
                                                /*              MODINUSE 298 */
#define NCP_K_MRESP_INVLMEVSRC          330

/* Define some macros for doing common things */

#define ADDR_COPY(a,b) \
        b[0] = a[0]; \
        b[1] = a[1]; \
        b[2] = a[2]; \
        b[3] = a[3]; \
        b[4] = a[4]; \
        b[5] = a[5];

#define ADDR_CMP(a,b) \
       (a[0] == b[0] && \
        a[1] == b[1] && \
        a[2] == b[2] && \
        a[3] == b[3] && \
        a[4] == b[4] && \
        a[5] == b[5])

#define SNAP_COPY(a,b) \
        b[0] = a[0]; \
        b[1] = a[1]; \
        b[2] = a[2]; \
        b[3] = a[3]; \
        b[4] = a[4];

#define SNAP_CMP(a,b) \
       (a[0] == b[0] && \
        a[1] == b[1] && \
        a[2] == b[2] && \
        a[3] == b[3] && \
        a[4] == b[4])


#endif /* AIM556_DEFH */
