//=============================================================================
//
// file :        AIM556.h
//
// description : Include file for the AIM556
//
// project :     MCA Canberra AIM 556
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
#include "AIM556_def.h"

#include <string>
#include <libnet.h>
#include <pcap.h>

// module_ownership_state

#define OWNERSHIP_UNKNOWN 0         /* module state is unknown */
#define OWNERSHIP_FREE    1         /* module is free */
#define OWNERSHIP_EXTERN  2         /* module is owned by an external process */
#define OWNERSHIP_LOCAL   3         /* module is owned localy */

class AIM556
{

public:

  // Default constructor
  // interface contains the network interface name (eg. eth0)
  // address contains the last two byte (eg. 0x08E0) of the full ethernet address 00:00:AF:00:XX:YY
  AIM556(std::string interface,std::string address);

  ~AIM556();

  // Initialise the module
  bool Init();

  std::string GetLastError();

  // AIM commands
  bool AcquireOwnership(bool override);
  bool ReleaseOwnership(bool override);
  bool GetAcquisitionState(int adc,int *state,int *live,int *real);
  bool SetAcquisitionState(int adc,int state);
  bool SetupAcquisition(int adc,int channel_nb,int plive,int preal,int mode);
  bool Clear(int adc,int size);
  bool ClearMem(int size);
  bool GetData(int adc,int channels,uint32 *buffer,int *live,int *real);
  bool Reset();

  // PCAP capture loop
  pcap_t *GetPCAP();
  void EtherCapture(const struct pcap_pkthdr *pkthdr, const unsigned char *buffer);

private:

  // Private method
  void Flush();
  bool SendCmd(int command, void *data, int dsize, int oflag);
  bool SendCmd(int command, void *data, int dsize, void *response,int rsize, int *size, int oflag, int *packet_code=NULL);
  bool Write(struct response_packet *pkt, int size);
  bool Read(struct response_packet *pkt, int size, int *actual);
  time_t get_ticks();

  // Inner stuff
  bool initialized;                       /* Initialized flag */
  std::string lastError;                  /* Last error message */
  uint8 module_add[6];                    /* Module MAC address */
  uint8 sys_add[6];                       /* Local eth MAC address */
  uint8 owner_add[6];                     /* Owner of module */
  uint8 zero_add[6];                      /* Not owned address */
  char hostname[8];                       /* Hostname */
  char *dev_name;                         /* Interface name, e.g. eth0 */
  int timeout_time;                       /* Timeout in millis */
  int header_size;                        /* The size of the device dependent header */
  int max_msg_size;                       /* Largest possible message size */
  unsigned char response_snap[SNAP_SIZE]; /* SNAP ID for normal messages */
  unsigned char status_snap[SNAP_SIZE];   /* SNAP ID for status/event messages */
  char ownership_state;                   /* Module ownership state */
  uint8 msg_count;                        /* counter of message */
  int sleep_time;                         /* Time in millis between command (avoid overload) */
  int retry_count;                        /* Number of retry in case of timeout */

  // libnet & pcap stuff
  libnet_t *net;                          /* libnet handle */
  pthread_t capture_pid;	          /* Ethernet capture thread id */   
  pcap_t   *pcap;                         /* PCAP handle */
  struct bpf_program pcap_filter;         /* PCAP filter */

  // Response message
  bool  message_present;               /* Message present */
  char *message_data;                  /* Message data */
  int   message_length;                /* Message length */
	
};
