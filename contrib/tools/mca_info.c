/* Definitions */

#include "nmc_sys_defs.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{

/*
 * Declarations
 */

	char *intfc;
 	int  s;

	if (argc < 2) {
		printf("Usage: %s <ethernet-interface>\n",argv[0]);
		printf("eg: %s eth0\n",argv[0]);
		exit(1); 
	}
	intfc = argv[1];
      
/*
* First, setup to perform network module I/O.
*/

	s=nmc_initialize(intfc);
	if(s == ERROR) return ERROR;


/*
* Show all modules on network
*/

	nmc_show_modules();
	
	//nmc_buy_modules();
	//icb_show_modules();

        return(OK);

}
