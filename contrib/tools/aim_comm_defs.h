/*******************************************************************************
*
* Acquisition Interface Module (AIM) Communications Definitions
*
*       This module contains definitions for the commands used by networked
*       data acquisition modules to talk to host computers. Note that the
*       basic communications protocol definitions are in NCP_COMM_DEFS.
*
* NOTE: Nearly all of these definitions are tied to the AIM firmware, i.e.
*       they define the format of the data packets to/from the AIM. Thus
*       these definitions cannot be changed unless the AIM firmware is changed.
*
********************************************************************************

/*
* Host to Module Command Definitions
*/

/*
* The command codes are found in ncp_comm_packet.packet_code.
* Following each code definition is its argument structure, and, if required,
* the normal module response code and structure structure.
*/

#ifndef AIM_COMM_DEFS
#define AIM_COMM_DEFS

/* Pack all structures on 1-byte boundaries */
#pragma pack(1)

typedef char int8;
typedef unsigned char uint8;

typedef short int16;
typedef unsigned short uint16;

typedef int int32;
typedef unsigned int uint32;

#define NCP_K_HCMD_SETACQADDR   1               /* SET ACQUISITION ADDRESS */
struct ncp_hcmd_setacqaddr {
        uint16 adc;                             /* ADC number */
        uint32 address;                         /* Acquisition address */
        uint32 limit;                           /* Acquisition limit address */
};

#define NCP_K_HCMD_SETELAPSED   2               /* SET ELAPSED TIMES */
struct ncp_hcmd_setelapsed {
        uint16 adc;
        uint32 live;                            /* Elapsed live time */
        uint32 real;                            /*         real      */
};

#define NCP_K_HCMD_SETMEMORY    3               /* SET MEMORY */
struct ncp_hcmd_setmemory {
        uint32 address;                         /* Destination address of data */
        uint32 size;                            /* Amount of data (bytes) */
                                                /* Start of data */
};

#define NCP_K_HCMD_SETMEMCMP    4               /* SET MEMORY COMPRESSED */
struct ncp_hcmd_setmemcmp {
        uint32 address;                         /* Destination address of data */
        uint32 size;                            /* Amount of data (bytes) */
                                                /* Start of data */
};

#define NCP_K_HCMD_SETPRESETS   5               /* SET PRESETS */
struct ncp_hcmd_setpresets {
        uint16 adc;
        uint32 live;                            /* Preset live time */
        uint32 real;                            /*        real      */
        uint32 totals;                          /* Preset total counts */
        uint32 start;                    	/* Preset totals start channel */
        uint32 end;                      	/*               end channel */
        uint32 limit;                    	/* Preset limit */
};

#define NCP_K_HCMD_SETACQSTATUS 6               /* SET ACQUISITION STATUS */
struct ncp_hcmd_setacqstate {
        uint16 adc;
        int8 status;                            /* New acquisition status (on/off) */
};

#define NCP_K_HCMD_ERASEMEM     7               /* ERASE MEMORY */
struct ncp_hcmd_erasemem {
        uint32 address;                  	/* Start address of memory to be erased */
        uint32 size;                     	/* Number of bytes to be erased */
};

#define NCP_K_HCMD_SETACQMODE   8               /* SET ACQUISITION MODE */
struct ncp_hcmd_setacqmode {
        uint16 adc;
        int8 mode;                              /* Acquisition mode; see NCP_C_AMODE_xxx */
};

#define NCP_K_HCMD_RETMEMORY    9               /* RETURN MEMORY */
struct ncp_hcmd_retmemory {
        uint32 address;                  	/* Start address of memory to return */
        uint32 size;                     	/* Number of bytes of memory to return */
};

#define NCP_K_HCMD_RETMEMCMP    10              /* RETURN MEMORY COMPRESSED */
struct ncp_hcmd_retmemcmp {
        uint32 address;                  	/* Start address of memory to return */
        uint32 size;                     	/* Number of bytes of memory to return */
};
#define NCP_K_MRESP_RETMEMCMP   227
struct ncp_mresp_retmemcmp {
        uint32 channels;                 	/* Number of channels of encoded data following */
                                                /* Encoded data starts here */
};

#define NCP_K_HCMD_RETADCSTATUS 11              /* RETURN ADC STATUS */
struct ncp_hcmd_retadcstatus {
        uint16 adc;
};
#define NCP_K_MRESP_ADCSTATUS   35
struct ncp_mresp_retadcstatus {
        int8 status;                            /* ADC on/off status */
        uint32 live;                     	/* Elapsed live time */
        uint32 real;                     	/*         real      */
        uint32 totals;                   	/*         total counts */
};

#define NCP_K_HCMD_SETHOSTMEM   13              /* SET HOST MEMORY */
struct ncp_hcmd_sethostmem {
        uint32 address;                  	/* Destination address of host data */
        uint32 size;                     	/* Amount of data (bytes) */
                                                /* Start of data */
};

#define NCP_K_HCMD_RETHOSTMEM   14              /* RETURN HOST MEMORY */
struct ncp_hcmd_rethostmem {
        uint32 address;                  	/* Start address of host memory to return */
        uint32 size;                     	/* Number of bytes of memory to return */
};

#define NCP_K_HCMD_SETOWNER     15              /* SET OWNER */
struct ncp_hcmd_setowner {
        uint8 owner_id[6];              	/* New owner ID */
        int8 owner_name[8];                     /* New owner name */
};

#define NCP_K_HCMD_SETOWNEROVER 16              /* SET OWNER OVERRIDE */
struct ncp_hcmd_setownerover {
        uint8 owner_id[6];              	/* New owner ID */
        int8 owner_name[8];                     /* New owner name */
};

#define NCP_K_HCMD_RESET        17              /* RESET MODULE */
#define NCP_K_HCMD_SETDISPLAY   18              /* SET DISPLAY CHARACTERISTICS */
#define NCP_K_HCMD_RETDISPLAY   19              /* RETURN DISPLAY */
#define NCP_K_HCMD_DIAGNOSE     20              /* DIAGNOSE */

#define NCP_K_HCMD_SENDICB      21              /* SEND TO ICB */
struct ncp_hcmd_sendicb {
        uint32 registers;                	/* Number of address/data pairs to write */
        struct {
           uint8 address;               	/* ICB address where data will be sent */
           uint8 data;                  	/* data to write */
        }  addresses[64];
};

#define NCP_K_HCMD_RECVICB      22              /* RECEIVE FROM ICB */
struct ncp_hcmd_recvicb {
        uint32 registers;                	/* Number of addresses to read */
        uint8 address[64];              	/* ICB addresses from which data is to be read */
};

#define NCP_K_HCMD_SETUPACQ     23              /* SETUP ACQUISITION */
struct ncp_hcmd_setupacq {
        uint16 adc;
        uint32 address;                  	/* Acquisition address */
        uint32 alimit;                   	/* Acquisition limit address */
        uint32 plive;                    	/* Preset live time */
        uint32 preal;                    	/*        real      */
        uint32 ptotals;                  	/* Preset total counts */
        uint32 start;                    	/* Preset totals start channel */
        uint32 end;                      	/*               end channel */
        uint32 plimit;                   	/* Preset limit */
        uint32 elive;                    	/* Elapsed live time */
        uint32 ereal;                    	/*         real      */
        int8 mode;                              /* Acquisition mode; see NCP_C_AMODE_xxx */
};

#define NCP_K_HCMD_RETACQSETUP  24              /* RETURN ACQUISITION SETUP INFO */
struct ncp_hcmd_retacqsetup {
        uint16 adc;
};
#define NCP_K_MRESP_RETACQSETUP 203
struct ncp_mresp_retacqsetup {
        uint32 address;                  	/* Acquisition address */
        uint32 alimit;                   	/* Acquisition limit address */
        uint32 plive;                    	/* Preset live time */
        uint32 preal;                    	/*        real      */
        uint32 ptotals;                  	/* Preset total counts */
        uint32 start;                    	/* Preset totals start channel */
        uint32 end;                      	/*               end channel */
        uint32 plimit;                   	/* Preset limit */
        int8 mode;
};

#define NCP_K_HCMD_SETMODEVSAP  25              /* SET MODULE EVENT MESSAGE ADDRESSES*/
struct ncp_hcmd_setmodevsap {
        uint16  mevsource;              	/* 0-255 are reserved for    */
                                                /* adc numbers. 255-65536    */
                                                /* are types NCP_K_MEVSRC_   */
        int8    mev_dsap;                       /* dest. service access point */
        int8    mev_ssap;                       /* sour. service access point */
        int8    snap_id[5];                     /* SNAP protocol ID */
};

#define NCP_K_HCMD_RETLISTMEM   26              /* RETURN LIST BUFFER */
struct ncp_hcmd_retlistmem {
        uint16 adc;                     	/* adc number */
        uint32 offset;                   	/* byte offset from start of buffer of transfer start */
        uint32 size;                     	/* number of bytes to return */
        int8 buffer;                            /* 0 for "1st" buffer, 1 for "2nd" */
};

#define NCP_K_HCMD_RELLISTMEM   27              /* RELEASE LIST BUFFER */
struct ncp_hcmd_rellistmem {
        uint16 adc;                     	/* adc number */
        int8 buffer;                            /* 0 for "1st" buffer, 1 for "2nd" */
};

#define NCP_K_HCMD_RETLISTSTAT  28              /* RETURN LIST ACQ STATUS */
struct ncp_hcmd_retliststat {
        uint16 adc;                     	/* adc number */
};
#define NCP_K_MRESP_RETLISTSTAT 251
struct ncp_mresp_retliststat {
        int8 status;                            /* acquire on/off state */
        int8 current_buffer;                    /* current acquire buffer: 0 for "1st" buffer, 1 for "2nd" */
        int8 buffer_1_full;                     /* 1st buffer status: 1 if full of data */
        uint32 offset_1;                 	/* number of bytes of data in 1st buffer */
        int8 buffer_2_full;                     /* 2nd buffer status: 1 if full of data */
        uint32 offset_2;                 	/* number of bytes of data in 2nd buffer */
};

#define NCP_K_HCMD_RETMEMSEP    29              /* RETURN MEMORY SEPARATED */
struct ncp_hcmd_retmemsep {
        uint32 address;                  	/* start address of first chunk of memory to return*/
        uint32 size;                     	/* number of bytes per chunk */
        uint32 offset;                   	/* byte offset between start of each chunk */
        uint32 chunks;                   	/* number of chunks to return */
};                     /* Note: total bytes returned will be */
                                                /*      size*chunks */

#define NCP_K_HCMD_RESETLIST    30              /* RESET LIST MODE */
struct ncp_hcmd_resetlist {
        uint16 adc;                     	/* adc number */
};

/*
* Define format for host memory usage. This is used by hosts to store module
* setup information in the module itself. There are 256 bytes of memory total.
*/

        struct nam_hostmem_v0_struct {

           int8 initialized;                    /* True if host mem has been setup */
           int8 version;                        /* Version number of this structure */
           int32 acqmem_usage_map[4];           /* Map of 8K blocks of acq memory */

           struct {
                int8 used;                      /* This input is used */
                int8 name[29];                  /* Configuration name of the input */
                int8 owner[12];                 /* Name of input's owner */
                int32 channels;                 /* Number of channels assigned to input */
                uint16 rows;            	/*           rows */
                uint8 groups;           	/*           groups */
                int32 memory_start;             /* Start of acq mem for this input */
                uint8 year;             	/* Acquisition start year (base = 1900) */
                uint8 month;            	/*                   month */
                uint8 day;              	/*                   day */
                uint8 hour;             	/*                   hour */
                uint8 minute;           	/*                   minute */
                uint8 seconds;          	/*                   seconds */
           } input[4];
        };

/*
* This structure defines the return argument from NMC_ACQU_GETACQSETUP; it
* characterizes the acquisition setup state of an AIM ADC.
*/

struct nmc_acqsetup_struct {

        int8 name[33];                          /* configuration name */
        int8 owner[12];                         /*               owner */
        int32 channels;
        int32 rows;
        int32 groups;
        int32 memory_start;                     /* acq memory start address */
        int32 plive;                            /* preset live time */
        int32 preal;                            /*        real      */
        int32 ptotals;                          /*        total counts */
        int32 start;                            /* preset totals start region */
        int32 end;                              /* preset totals end region */
        int32 plimit;                           /* preset limit */
        int32 mode;                             /* acquisition mode */
        int32 astime[2];                        /* acqusition start date/time */
        int32 agroup;                           /* acquire group */
        int32 status;                           /* acquire status */
        int32 live;                             /* elapsed live time */
        int32 real;                             /*         real      */
        int32 totals;                           /*         total counts */
};

/* Revert to previous packing */
#pragma pack()

/*
* Miscellaneous definitions
*/

/*
* Module acquisition modes
*/

#define NCP_C_AMODE_PHA         1               /* PHA */
#define NCP_C_AMODE_PHALFC      2               /* PHA/LFC */
#define NCP_C_AMODE_DLIST       3               /* Double buffered list */

#endif /*AIM_COMM_DEFS*/

