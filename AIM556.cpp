//=============================================================================
//
// file :        AIM556.cpp
//
// description : Source code for the AIM556 control
//
// project :     MCA Canberra AIM 556
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================

#include "AIM556.h"
#include <stdlib.h>

using namespace std;

// ============================================================================

AIM556::AIM556(string interface,string address) {

  initialized = false;
  lastError = "";
  long add = strtol(address.c_str(),NULL,16);

  module_add[0] = 0x00;
  module_add[1] = 0x00;
  module_add[2] = 0xAF;
  module_add[3] = 0x00;
  module_add[4] = (add & 0xFF00) >> 8;
  module_add[5] = add & 0xFF;

  dev_name = strdup( interface.c_str() );
	
}

// ============================================================================

AIM556::~AIM556() {

  // Free all
  free(dev_name);

  if( initialized ) {
    if( !ReleaseOwnership(true) ) {
      fprintf(stderr,"Warning, ReleaseOwnership() failed :%s\n",lastError.c_str());
    }
    Flush();
    //pcap_breakloop(pcap);
    //pcap_freecode(&pcap_filter);
    //pcap_close(pcap);
    //pthread_join(capture_pid,NULL);
    libnet_destroy(net);
  }

}

// ============================================================================

string AIM556::GetLastError() {

  return lastError;

}

// ============================================================================

pcap_t *AIM556::GetPCAP() {

  return pcap;

}

// ============================================================================

// PCAP callback
void _etherCapture(unsigned char *usrdata, const struct pcap_pkthdr *pkthdr, const unsigned char *buffer)
{

  AIM556 *obj = (AIM556 *)usrdata;
  obj->EtherCapture(pkthdr,buffer);

}

// Thread proc
void _captureThread(AIM556 *obj) {

  pcap_loop(obj->GetPCAP(),-1,(pcap_handler)_etherCapture,(unsigned char*)obj);
  fprintf(stderr,"AIM556::etherCapture() : pcap_loop terminated\n");

}

// ============================================================================

bool AIM556::Init() {

    if( initialized )
      return true;
   
    // These setting are good for a MCA AIM 556 connected through a 10Mbps network

    timeout_time = 2000;            /* timeout in millis */
    max_msg_size = NMC_K_MAX_NIMSG; /* Maximum Ethernet message size for the MCA */
    sleep_time = 50000;             /* sleep time in micro (avoid AIM overload) */
    retry_count = 2;                /* Maximun retry count */
    msg_count = 0;

    // Initialise ownership address and state
    memset(&owner_add,0,sizeof(owner_add));
    memset(&zero_add,0,sizeof(zero_add));
    ownership_state = OWNERSHIP_UNKNOWN;

    // Initialise host name
    char hostname_full[256];
    gethostname(hostname_full, sizeof(hostname_full));
    strncpy(hostname, hostname_full, sizeof(hostname));
    for(int i=strlen(hostname); i<(int)sizeof(hostname); i++)
      hostname[i] = ' ';

    // Set up libpcap

    char pcaperrbuf[PCAP_ERRBUF_SIZE];
    char filterstr[256];

    sprintf( filterstr, "ether[6]=0 and ether[7]=0 and ether[8]=0xaf and ether[9]=0 and ether[10]=0x%x and ether[11]=0x%x" ,
              module_add[4] ,  module_add[5] );

    pcap = pcap_create(dev_name,pcaperrbuf);
    if (pcap==NULL) {
      lastError = "AIM556::Init() : pcap_create on " + string(dev_name) + " failed: " + string(pcaperrbuf);
      return false;
    }
    if( pcap_set_immediate_mode(pcap,1) != 0 ) {
      lastError = "AIM556::Init() : pcap_set_immediate_mode failed: " + string(pcap_geterr(pcap));
      return false;
    }
    if( pcap_activate(pcap) != 0 ) {
      lastError = "AIM556::Init() : pcap_activate failed: " + string(pcap_geterr(pcap));
      return false;
    }

    if( pcap_compile(pcap,&pcap_filter,filterstr,0,0) != 0 ) {
      lastError = "AIM556::Init() : pcap_compile failed: " + string(pcap_geterr(pcap)); 
      return false;
    }
    if (pcap_setfilter(pcap,&pcap_filter) != 0 ){
      lastError = "AIM556::Init() : pcap_setfilter failed: " + string(pcap_geterr(pcap));
      return false;
    }

    // Set up libnet
    char neterrbuf[LIBNET_ERRBUF_SIZE];
    net = libnet_init(LIBNET_LINK, dev_name, neterrbuf);
    if(net==NULL) {
      lastError = "AIM556::Init() : libnet_init() failed: " + string(neterrbuf);
      return false;
    }

    // Get ethernet Mac-Address of the device
    struct libnet_ether_addr *hw_address;  
    if ((hw_address = libnet_get_hwaddr(net)) == NULL) {
      lastError = "AIM556::Init() : libnet_get_hwaddr() failed " +  string(libnet_geterror(net));
      return false;
    }
    ADDR_COPY(hw_address->ether_addr_octet, sys_add);

    fprintf(stderr,"AIM556::Init() : MAC Src: %02x:%02x:%02x:%02x:%02x:%02x\n",
              sys_add[0],sys_add[1],sys_add[2],sys_add[3],sys_add[4],sys_add[5]);

    // Use the "extended 802" protocol (SNAP), using as our ID
    // 00-00-AF-xx-yy, where xx-yy are this process's PID.
    // For status and event messages use the same ID but set the top bit.

    long pid = (long)getpid();

    response_snap[0] = 0;
    response_snap[1] = 0;
    response_snap[2] = 0xAF;             /* Nuclear Data company code*/
    memcpy(&response_snap[3], &pid, 2);  /* Copy the first word of the process ID */
    response_snap[4] &= 0x7f;            /* clear msbit so response and status differ */

    header_size = sizeof(struct enet_header) + sizeof(struct snap_header);

    // The SNAP protocol ID for status and event messages
    // is the same as before except that the top bit of the last byte is set.
    SNAP_COPY(response_snap, status_snap);
    status_snap[4] |= 0x80;

    message_present=false;
    message_data=NULL;
    message_length=0;
    
    // Launch pcap loop
    pthread_create(&capture_pid, NULL,(void * (*)(void *))_captureThread, (void*)this);

    usleep(100000);

    int max_chans = ( max_msg_size - sizeof(struct ncp_comm_header) - sizeof(struct ncp_comm_packet) )/4;
    fprintf(stderr,"Maximum number of channels per ethernet packets:%d\n",max_chans);

    initialized = true;
    return true;

}

// ============================================================================

void AIM556::Flush() {

  if(message_data) free(message_data);
  message_present=0;
  message_data=NULL;
  message_length=0;

}

// ============================================================================

void AIM556::EtherCapture(const struct pcap_pkthdr *pkthdr, const unsigned char *buffer) {

    struct enet_header *h;
    struct snap_header *s;
    int    length;

    h = (struct enet_header *) buffer;
    s = (struct snap_header *) (h + 1);
    length = pkthdr->caplen;

    if (SNAP_CMP(s->snap_id, status_snap)) {

      // Status SNAP ID
      // Should never get there, we do not use inquiry request or event request
      fprintf(stderr,"AIM556::EtherCapture() : Received status message\n");

    } else if (SNAP_CMP(s->snap_id, response_snap)) {

      // Response SNAP ID
      if(message_data) free(message_data);
      message_data = (char *)malloc(length);
      memcpy( message_data , buffer , length );
      message_length=length;
      message_present=true;

    } else {

      fprintf(stderr,"AIM556::EtherCapture() : Unrecognized SNAP ID, Src: %02x:%02x:%02x:%02x:%02x:%02x Dst: %02x:%02x:%02x:%02x:%02x:%02x\n",
              h->source[0],h->source[1],h->source[2],h->source[3],h->source[4],h->source[5],
              h->dest[0],h->dest[1],h->dest[2],h->dest[3],h->dest[4],h->dest[5]);

    }
    return;


}

// ============================================================================

bool AIM556::Write(struct response_packet *pkt, int size) {

  SNAP_COPY(response_snap, pkt->snap_header.snap_id);

  pkt->snap_header.dsap = LLC_SAP_SNAPEXT;
  pkt->snap_header.ssap = LLC_SAP_SNAPEXT;
  pkt->snap_header.control = 0x03;
  int length = size + sizeof(struct snap_header);

  libnet_clear_packet(net);

  if ( libnet_build_ethernet(module_add,
                             sys_add,
                             length, 
                             (unsigned char *)&(pkt->snap_header),
                             length, net, 0) == -1) {

    lastError = "AIM556::Send() libnet_build_ethernet failed : " + string(libnet_geterror(net));
    return false;
  }

#if 0
  // Dump packet
  uint32_t len;
  uint8_t *packet = NULL;
  libnet_pblock_coalesce(net, &packet, &len);
  libnet_diag_dump_hex( packet , len , 1 , stdout );
  if (net->aligner > 0) {
    packet = packet - net->aligner;
  }
  free(packet);
#endif

  int ret = libnet_write(net);
  if( ret<0 ) {
    lastError = "AIM556::Send() libnet_write failed : " + string(libnet_geterror(net));
    return false;
  }

  return true;

}

// ============================================================================

bool AIM556::Read(struct response_packet *pkt, int size, int *actual) {

    int len, timeout, towait;
    struct enet_header *e;
    struct ncp_comm_header *p;

    timeout = timeout_time;
    towait = 1;

    usleep(1000);
    while(timeout>0 && !message_present) {
     usleep(towait*1000);
     timeout -= towait;
     if(towait<128) towait = towait << 1;
    }

    if (!message_present) {

      lastError = "AIM556::Read() failed : timeout occured";
      return false;

    } else {

      memcpy( pkt , message_data , message_length );
      len = message_length;

    }

    // Update ownership field
    p = &pkt->ncp_comm_header;
    ADDR_COPY(p->owner_id, owner_add);

    if( ADDR_CMP( owner_add, sys_add )) {
      ownership_state = OWNERSHIP_LOCAL;
    } else if (ADDR_CMP( owner_add, zero_add )) {
      ownership_state = OWNERSHIP_FREE;
    } else {
      ownership_state = OWNERSHIP_EXTERN;
    }

    *actual = len;
    return true;

}

// ============================================================================
bool AIM556::SendCmd(int command, void *data, int dsize, int oflag) {

  return SendCmd( command , data , dsize , NULL , 0 , NULL , oflag , NULL );

}

bool AIM556::SendCmd(int command, void *data, int dsize, void *response,int rsize, int *size, int oflag, int *packet_code)
{

    int rmsgsize,cmdsize;
    unsigned char *d;
    struct ncp_comm_header *h=NULL;
    struct ncp_comm_packet *p=NULL;
    struct response_packet in_pkt;
    struct response_packet out_pkt;

    // Check message size
    if(dsize > (max_msg_size - (int)sizeof(*h) - (int)sizeof(*p))) {
      lastError = "AIM556::SendCmd() : message too big";
      return false;
    }

    // If the module is not owned by us, return an error unless OFLAG is true.
    if( ownership_state != OWNERSHIP_LOCAL && !oflag ) {
      lastError = "AIM556::SendCmd() : Acquire ownership failed, please reinitialise";
      return false;      
    }

    time_t t0 = get_ticks();

    // Build the protocol and command packet headers, and copy the command arguments
    // into the packet data area.
    h = &out_pkt.ncp_comm_header;
    p = &out_pkt.ncp_comm_packet;
    d = &out_pkt.ncp_packet_data[0];

    int s = sizeof(*h) + sizeof(*p);
    memset(h, 0, s);
    h->checkword = NCP_K_CHECKWORD;
    h->protocol_type = NCP_C_PRTYPE_NAM;
    h->message_type = NCP_C_MSGTYPE_PACKET;

    // advance the current message number
    msg_count++;       
    h->message_number = msg_count;
    h->data_size = sizeof(*p) + dsize;
    cmdsize = sizeof(*h) + h->data_size;
    p->packet_size = dsize;
    p->packet_type = NCP_C_PTYPE_HCOMMAND;
    p->packet_code = command;

    if(data!=NULL) memcpy(d, data, dsize);

    // make sure there are no queued messages
    int retry = retry_count;
    bool ok = false;
    while( !ok && retry>0 ) {

      Flush();

      // Send the packet
      if( !Write( &out_pkt, cmdsize ) )
        return false;

      // Read response
      ok = Read( &in_pkt , sizeof(struct response_packet) , &rmsgsize);

      if( !ok ) {
        // Timeout occured
        usleep(sleep_time);
        retry--;
      }

    }

    if( !ok )
      return false;

    // Check response
    h = &in_pkt.ncp_comm_header;
    p = &in_pkt.ncp_comm_packet;
    if(h->message_number != msg_count ||
       h->checkword != (int32)NCP_K_CHECKWORD ||
       h->protocol_type != NCP_C_PRTYPE_NAM ||
       p->packet_type != NCP_C_PTYPE_MRESPONSE) {

      lastError = "AIM556::SendCmd() : Invalid message received";
      return false;
    }

    if( size!=NULL && rsize>0 ) {

      *size = p->packet_size;
      if(*size > rsize) *size = rsize;
      if(*size!=0) memcpy(response, &in_pkt.ncp_packet_data, *size);
    
    }

    if( packet_code!=NULL )
      *packet_code = p->packet_code;

    time_t t1 = get_ticks();

    //printf("AIM556::SendCmd(): %d ok (%d ms)\n",command,(t1-t0));
    return true;

}

// ============================================================================

bool AIM556::AcquireOwnership(bool override) {

   // Reset the module
   if( !Reset() )
     return false;

   // Acquire ownership
   int command;
   struct ncp_hcmd_setowner packet;

   if(ownership_state == OWNERSHIP_LOCAL) 
     return true;

   // Build the command packet
   ADDR_COPY(sys_add, packet.owner_id);
   memcpy( packet.owner_name , hostname , 8 );

   // Send it
   if (override) command = NCP_K_HCMD_SETOWNEROVER;
   else command = NCP_K_HCMD_SETOWNER;
   
   usleep(sleep_time);
   if( !SendCmd(command,&packet,sizeof(packet),1) )
     return false;

   return true;

}

// ============================================================================

bool AIM556::ReleaseOwnership(bool override) {

   // Reset the module
   if( !Reset() )
     return false;

   // Release ownership
   int command;
   struct ncp_hcmd_setowner packet;

   // Build the command packet
   ADDR_COPY(zero_add, packet.owner_id);
   memset( packet.owner_name , 32 , 8 );

   // Send it
   if (override) command = NCP_K_HCMD_SETOWNEROVER;
   else command = NCP_K_HCMD_SETOWNER;

   usleep(sleep_time);
   if( !SendCmd(command,&packet,sizeof(packet),1) )
     return false;

   return true;

}

// ============================================================================

bool AIM556::SetAcquisitionState(int adc,int state) {

   struct ncp_hcmd_setacqstate setstate;
   setstate.adc = adc;
   setstate.status = state;

   // Send it!
   usleep(sleep_time);
   return SendCmd(NCP_K_HCMD_SETACQSTATUS,&setstate,sizeof(setstate),0);

}

// ============================================================================

bool AIM556::GetAcquisitionState(int adc,int *state,int *live,int *real) {

   struct ncp_hcmd_retadcstatus getstatus;
   struct ncp_mresp_retadcstatus retstatus;
   int respsize,code;

   // Get the acquire status and elapsed times. Note that this is not correct
   // for GAR or MX mode configurations.

   getstatus.adc = adc;

   usleep(sleep_time);
   if( !SendCmd(NCP_K_HCMD_RETADCSTATUS,
               &getstatus,sizeof(getstatus),&retstatus,
               sizeof(retstatus),&respsize,0,&code) ) {
     return false;
   }

   if(code != NCP_K_MRESP_ADCSTATUS) {      
      lastError = "AIM556::SendCmd() : Unexpected message received (NCP_K_MRESP_ADCSTATUS)";
      return false;
   }

   *live = retstatus.live;
   *real = retstatus.real;
   //*totals = retstatus.totals;
   if(retstatus.status) *state = 1; else *state = 0;
   
   return true;

}

// ============================================================================

bool AIM556::SetupAcquisition(int adc,int channel_nb,int plive,int preal,int mode) {

   struct ncp_hcmd_setupacq setup;
   memset(&setup,0,sizeof(setup));

   // Build the command message
   setup.adc = adc;
   setup.address = 0;
   setup.alimit = setup.address + channel_nb*4 - 1;
   setup.plive = plive;
   setup.preal = preal;
   setup.mode = mode;
   

   printf("SetupAcquisition(adc:%d,channel,%d,plive=%d,preal=%d,mode=%d)\n",adc,channel_nb,plive,preal,mode);

   // Send it!
   usleep(sleep_time);
   return SendCmd(NCP_K_HCMD_SETUPACQ,&setup,sizeof(setup),0);

}

// ============================================================================

bool AIM556::Clear(int adc,int size) {

   if(!Reset())
     return false;

   // Clear elapsed time
   struct ncp_hcmd_setelapsed elapsed;
   elapsed.adc=adc;
   elapsed.live=0;
   elapsed.real=0;

   // Send it!
   usleep(sleep_time);
   if( !SendCmd(NCP_K_HCMD_SETELAPSED,&elapsed,sizeof(elapsed),0) )
     return false;

   // Clear mem
   struct ncp_hcmd_erasemem erase;
   erase.address = 0;
   erase.size = size;

   // Send it!
   usleep(sleep_time);
   return SendCmd(NCP_K_HCMD_ERASEMEM,&erase,sizeof(erase),0);

}

// ============================================================================

bool AIM556::ClearMem(int size) {

   // Clear mem
   struct ncp_hcmd_erasemem erase;
   erase.address = 0;
   erase.size = size;

   // Send it!
   usleep(sleep_time);
   return SendCmd(NCP_K_HCMD_ERASEMEM,&erase,sizeof(erase),0);

}

// ============================================================================

bool AIM556::Reset() {

   // Send it!
   usleep(sleep_time);
   bool ok =  SendCmd(NCP_K_HCMD_RESET,NULL,0,1);
   return ok;

}

// ============================================================================

bool AIM556::GetData(int adc,int channels,uint32 *buffer,int *live,int *real) {
 
  struct ncp_hcmd_retmemory retmemory;

  // Determine how many channels can be returned per message from the module

  int max_chans = ( max_msg_size - sizeof(struct ncp_comm_header) - sizeof(struct ncp_comm_packet) )/4;

  // Now transfer the data. Basically, each time through the loop, we just get 
  // either the number of channels left or the max/message, whichever is smaller.

  int chans_left = channels;
  char *mem_buffer = (char *)buffer;
 
  usleep(sleep_time);
  int iter=0;

  while (chans_left > 0) {

     int cur_chans = chans_left;
     if(cur_chans > max_chans) cur_chans = max_chans;

      // compute source address and size
      retmemory.address = (channels - chans_left) * 4; 
      retmemory.size = cur_chans * 4;
      int max_bytes = chans_left * 4;      
      int pcode,actual;

      if( !SendCmd(NCP_K_HCMD_RETMEMORY, &retmemory, sizeof(retmemory), mem_buffer, max_bytes, &actual, 0, &pcode) ) {
        fprintf(stderr,"AIM556::GetData() failed at iteration %d: %s\n",iter,lastError.c_str());
        return false;
      }

      if(pcode != NCP_K_HCMD_RETMEMORY) {
        lastError = "AIM556::GetData() : Unexpected message received (NCP_K_HCMD_RETMEMORY)";
        return false;
      }

      if(actual == 0) {
        lastError = "AIM556::GetData() : zero length message received";
        return false;
      }

      chans_left -= actual/4;
      mem_buffer += (actual & ~3);
      iter++;

   }

   int state;
   if( !GetAcquisitionState(adc,&state,live,real) )
     return false;

   return true;

}

time_t AIM556::get_ticks() {

  static time_t tickStart = -1;
  if (tickStart < 0)
    tickStart = time(NULL);

  struct timeval tv;
  gettimeofday(&tv, NULL);
  return ((tv.tv_sec - tickStart) * 1000 + tv.tv_usec / 1000);

}
