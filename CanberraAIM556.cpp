/*----- PROTECTED REGION ID(CanberraAIM556.cpp) ENABLED START -----*/
static const char *RcsId = "$Id:  $";
//=============================================================================
//
// file :        CanberraAIM556.cpp
//
// description : C++ source for the CanberraAIM556 class and its commands.
//               The class is derived from Device. It represents the
//               CORBA servant object which will be accessed from the
//               network. All commands which can be executed on the
//               CanberraAIM556 are implemented in this file.
//
// project :     MCA Canberra AIM 556
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#include <CanberraAIM556.h>
#include <CanberraAIM556Class.h>

/*----- PROTECTED REGION END -----*/	//	CanberraAIM556.cpp

/**
 *  CanberraAIM556 class description:
 *    A class to control the MCA Canberra AIM 556 (A ou B).
 */

//================================================================
//  The following table gives the correspondence
//  between command and method names.
//
//  Command name  |  Method name
//================================================================
//  State         |  dev_state
//  Status        |  Inherited (no method)
//  On            |  on
//  Off           |  off
//  Reset         |  reset
//  ClearMemory   |  clear_memory
//================================================================

//================================================================
//  Attributes managed are:
//================================================================
//  ADC             |  Tango::DevShort	Scalar
//  Mode            |  Tango::DevEnum	Scalar
//  Channels        |  Tango::DevULong	Scalar
//  PresetLiveTime  |  Tango::DevDouble	Scalar
//  PresetRealTime  |  Tango::DevDouble	Scalar
//  LiveTime        |  Tango::DevDouble	Scalar
//  RealTime        |  Tango::DevDouble	Scalar
//  Data            |  Tango::DevULong	Spectrum  ( max = 65536)
//================================================================

namespace CanberraAIM556_ns
{
/*----- PROTECTED REGION ID(CanberraAIM556::namespace_starting) ENABLED START -----*/

//	static initializations

/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::namespace_starting

//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::CanberraAIM556()
 *	Description : Constructors for a Tango device
 *                implementing the classCanberraAIM556
 */
//--------------------------------------------------------
CanberraAIM556::CanberraAIM556(Tango::DeviceClass *cl, string &s)
 : TANGO_BASE_CLASS(cl, s.c_str())
{
	/*----- PROTECTED REGION ID(CanberraAIM556::constructor_1) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::constructor_1
}
//--------------------------------------------------------
CanberraAIM556::CanberraAIM556(Tango::DeviceClass *cl, const char *s)
 : TANGO_BASE_CLASS(cl, s)
{
	/*----- PROTECTED REGION ID(CanberraAIM556::constructor_2) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::constructor_2
}
//--------------------------------------------------------
CanberraAIM556::CanberraAIM556(Tango::DeviceClass *cl, const char *s, const char *d)
 : TANGO_BASE_CLASS(cl, s, d)
{
	/*----- PROTECTED REGION ID(CanberraAIM556::constructor_3) ENABLED START -----*/
	init_device();
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::constructor_3
}

//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::delete_device()
 *	Description : will be called at device destruction or at init command
 */
//--------------------------------------------------------
void CanberraAIM556::delete_device()
{
	DEBUG_STREAM << "CanberraAIM556::delete_device() " << device_name << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::delete_device) ENABLED START -----*/
	
	//	Delete device allocated objects
	delete aim;

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::delete_device
	delete[] attr_ADC_read;
	delete[] attr_Mode_read;
	delete[] attr_Channels_read;
	delete[] attr_PresetLiveTime_read;
	delete[] attr_PresetRealTime_read;
	delete[] attr_LiveTime_read;
	delete[] attr_RealTime_read;
	delete[] attr_Data_read;
}

//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::init_device()
 *	Description : will be called at device initialization.
 */
//--------------------------------------------------------
void CanberraAIM556::init_device()
{
	DEBUG_STREAM << "CanberraAIM556::init_device() create device " << device_name << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::init_device_before) ENABLED START -----*/
	
	//	Initialization before get_device_property() call
        aim = NULL;
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::init_device_before
	

	//	Get the device properties from database
	get_device_property();
	
	attr_ADC_read = new Tango::DevShort[1];
	attr_Mode_read = new ModeEnum[1];
	attr_Channels_read = new Tango::DevULong[1];
	attr_PresetLiveTime_read = new Tango::DevDouble[1];
	attr_PresetRealTime_read = new Tango::DevDouble[1];
	attr_LiveTime_read = new Tango::DevDouble[1];
	attr_RealTime_read = new Tango::DevDouble[1];
	attr_Data_read = new Tango::DevULong[65536];
	/*----- PROTECTED REGION ID(CanberraAIM556::init_device) ENABLED START -----*/

  //	Initialize device
  aim = new AIM556(interface,address);

  if( !aim->Init() ) {
    Tango::Except::throw_exception(
            (const char *)"CanberraAIM556::error_init",
            (const char *)aim->GetLastError().c_str(),
            (const char *)"CanberraAIM556::init_device");
  }

	if( !aim->AcquireOwnership(true) ) {
		cerr << "Warning, AcquireOwnership failed" << aim->GetLastError() << endl;
	}

	// Default values
	attr_ADC_read[0] = 0;
	attr_Mode_read[0] = _ModeEnum::_PHA;
	attr_Channels_read[0] = 8192;
	attr_PresetLiveTime_read[0] = 3600;
	attr_PresetRealTime_read[0] = 3600;
	memset(attr_Data_read,0,sizeof(Tango::DevULong)*65536);
	
	// Initialise setpoint
	double v;
  if( InitSetPoint("ADC",&v) )
    attr_ADC_read[0] = (Tango::DevShort)v;
  if( InitSetPoint("Channels",&v) )
    attr_Channels_read[0] = (Tango::DevULong)v;
  if( InitSetPoint("Mode",&v) )
    attr_Mode_read[0] = (_ModeEnum)v;
  if( InitSetPoint("PresetLiveTime",&v) )
    attr_PresetLiveTime_read[0] = v;
  if( InitSetPoint("PresetRealTime",&v) )
    attr_PresetRealTime_read[0] = v;

  SetupMCA();

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::init_device
}

//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::get_device_property()
 *	Description : Read database to initialize property data members.
 */
//--------------------------------------------------------
void CanberraAIM556::get_device_property()
{
	/*----- PROTECTED REGION ID(CanberraAIM556::get_device_property_before) ENABLED START -----*/
	
	//	Initialize property data members
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::get_device_property_before


	//	Read device properties from database.
	Tango::DbData	dev_prop;
	dev_prop.push_back(Tango::DbDatum("Address"));
	dev_prop.push_back(Tango::DbDatum("Interface"));

	//	is there at least one property to be read ?
	if (dev_prop.size()>0)
	{
		//	Call database and extract values
		if (Tango::Util::instance()->_UseDb==true)
			get_db_device()->get_property(dev_prop);
	
		//	get instance on CanberraAIM556Class to get class property
		Tango::DbDatum	def_prop, cl_prop;
		CanberraAIM556Class	*ds_class =
			(static_cast<CanberraAIM556Class *>(get_device_class()));
		int	i = -1;

		//	Try to initialize Address from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  address;
		else {
			//	Try to initialize Address from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  address;
		}
		//	And try to extract Address value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  address;

		//	Try to initialize Interface from class property
		cl_prop = ds_class->get_class_property(dev_prop[++i].name);
		if (cl_prop.is_empty()==false)	cl_prop  >>  interface;
		else {
			//	Try to initialize Interface from default device value
			def_prop = ds_class->get_default_device_property(dev_prop[i].name);
			if (def_prop.is_empty()==false)	def_prop  >>  interface;
		}
		//	And try to extract Interface value from database
		if (dev_prop[i].is_empty()==false)	dev_prop[i]  >>  interface;

	}

	/*----- PROTECTED REGION ID(CanberraAIM556::get_device_property_after) ENABLED START -----*/
	
	//	Check device property data members init
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::get_device_property_after
}

//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::always_executed_hook()
 *	Description : method always executed before any command is executed
 */
//--------------------------------------------------------
void CanberraAIM556::always_executed_hook()
{
	DEBUG_STREAM << "CanberraAIM556::always_executed_hook()  " << device_name << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::always_executed_hook) ENABLED START -----*/
	
	//	code always executed before all requests
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::always_executed_hook
}

//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::read_attr_hardware()
 *	Description : Hardware acquisition for attributes
 */
//--------------------------------------------------------
void CanberraAIM556::read_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "CanberraAIM556::read_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_attr_hardware
}
//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::write_attr_hardware()
 *	Description : Hardware writing for attributes
 */
//--------------------------------------------------------
void CanberraAIM556::write_attr_hardware(TANGO_UNUSED(vector<long> &attr_list))
{
	DEBUG_STREAM << "CanberraAIM556::write_attr_hardware(vector<long> &attr_list) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::write_attr_hardware) ENABLED START -----*/
	
	//	Add your own code
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::write_attr_hardware
}

//--------------------------------------------------------
/**
 *	Read attribute ADC related method
 *	Description: ADC index (0..1)
 *
 *	Data type:	Tango::DevShort
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::read_ADC(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::read_ADC(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_ADC) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_ADC_read);
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_ADC
}
//--------------------------------------------------------
/**
 *	Write attribute ADC related method
 *	Description: ADC index (0..1)
 *
 *	Data type:	Tango::DevShort
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::write_ADC(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::write_ADC(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevShort	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(CanberraAIM556::write_ADC) ENABLED START -----*/
	attr_ADC_read[0] = w_val;
  SetupMCA();

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::write_ADC
}
//--------------------------------------------------------
/**
 *	Read attribute Mode related method
 *	Description: Acquisition Mode
 *               0 -> PHA
 *               1 -> PHA LFC
 *               2 -> DBL BUFFERS
 *
 *	Data type:	Tango::DevEnum (ModeEnum)
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::read_Mode(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::read_Mode(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_Mode) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_Mode_read);
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_Mode
}
//--------------------------------------------------------
/**
 *	Write attribute Mode related method
 *	Description: Acquisition Mode
 *               0 -> PHA
 *               1 -> PHA LFC
 *               2 -> DBL BUFFERS
 *
 *	Data type:	Tango::DevEnum (ModeEnum)
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::write_Mode(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::write_Mode(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	ModeEnum	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(CanberraAIM556::write_Mode) ENABLED START -----*/
	attr_Mode_read[0] = w_val;
  SetupMCA();

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::write_Mode
}
//--------------------------------------------------------
/**
 *	Read attribute Channels related method
 *	Description: Channel number
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::read_Channels(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::read_Channels(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_Channels) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_Channels_read);
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_Channels
}
//--------------------------------------------------------
/**
 *	Write attribute Channels related method
 *	Description: Channel number
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::write_Channels(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::write_Channels(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevULong	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(CanberraAIM556::write_Channels) ENABLED START -----*/
	attr_Channels_read[0] = w_val;
  SetupMCA();

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::write_Channels
}
//--------------------------------------------------------
/**
 *	Read attribute PresetLiveTime related method
 *	Description: Preset live time
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::read_PresetLiveTime(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::read_PresetLiveTime(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_PresetLiveTime) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_PresetLiveTime_read);
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_PresetLiveTime
}
//--------------------------------------------------------
/**
 *	Write attribute PresetLiveTime related method
 *	Description: Preset live time
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::write_PresetLiveTime(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::write_PresetLiveTime(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(CanberraAIM556::write_PresetLiveTime) ENABLED START -----*/
	attr_PresetLiveTime_read[0] = w_val;
  SetupMCA();

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::write_PresetLiveTime
}
//--------------------------------------------------------
/**
 *	Read attribute PresetRealTime related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::read_PresetRealTime(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::read_PresetRealTime(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_PresetRealTime) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_PresetRealTime_read);
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_PresetRealTime
}
//--------------------------------------------------------
/**
 *	Write attribute PresetRealTime related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::write_PresetRealTime(Tango::WAttribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::write_PresetRealTime(Tango::WAttribute &attr) entering... " << endl;
	//	Retrieve write value
	Tango::DevDouble	w_val;
	attr.get_write_value(w_val);
	/*----- PROTECTED REGION ID(CanberraAIM556::write_PresetRealTime) ENABLED START -----*/
	attr_PresetRealTime_read[0] = w_val;
	SetupMCA();

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::write_PresetRealTime
}
//--------------------------------------------------------
/**
 *	Read attribute LiveTime related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::read_LiveTime(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::read_LiveTime(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_LiveTime) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_LiveTime_read);
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_LiveTime
}
//--------------------------------------------------------
/**
 *	Read attribute RealTime related method
 *	Description: 
 *
 *	Data type:	Tango::DevDouble
 *	Attr type:	Scalar
 */
//--------------------------------------------------------
void CanberraAIM556::read_RealTime(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::read_RealTime(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_RealTime) ENABLED START -----*/
	//	Set the attribute value
	attr.set_value(attr_RealTime_read);
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_RealTime
}
//--------------------------------------------------------
/**
 *	Read attribute Data related method
 *	Description: 
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Spectrum max = 65536
 */
//--------------------------------------------------------
void CanberraAIM556::read_Data(Tango::Attribute &attr)
{
	DEBUG_STREAM << "CanberraAIM556::read_Data(Tango::Attribute &attr) entering... " << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::read_Data) ENABLED START -----*/

  int live;
  int real;

  if( !aim->GetData(attr_ADC_read[0],attr_Channels_read[0],attr_Data_read,&live,&real) ) {
    Tango::Except::throw_exception(
            (const char *)"CanberraAIM556::error_read",
            (const char *)aim->GetLastError().c_str(),
            (const char *)"CanberraAIM556::read_Data");
  }

  attr.set_value(attr_Data_read, attr_Channels_read[0]);
  attr_LiveTime_read[0] = (double)live / 100.0;
  attr_RealTime_read[0] = (double)real / 100.0;

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::read_Data
}

//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::add_dynamic_attributes()
 *	Description : Create the dynamic attributes if any
 *                for specified device.
 */
//--------------------------------------------------------
void CanberraAIM556::add_dynamic_attributes()
{
	/*----- PROTECTED REGION ID(CanberraAIM556::add_dynamic_attributes) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic attributes if any
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::add_dynamic_attributes
}

//--------------------------------------------------------
/**
 *	Command State related method
 *	Description: This command gets the device state (stored in its device_state data member) and returns it to the caller.
 *
 *	@returns Device state
 */
//--------------------------------------------------------
Tango::DevState CanberraAIM556::dev_state()
{
	DEBUG_STREAM << "CanberraAIM556::State()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::dev_state) ENABLED START -----*/

  Tango::DevState	argout = Tango::UNKNOWN;
  int state,live,real;

  if( !aim->GetAcquisitionState(attr_ADC_read[0],&state,&live,&real) ) {
    Tango::Except::throw_exception(
            (const char *)"CanberraAIM556::error_read",
            (const char *)aim->GetLastError().c_str(),
            (const char *)"CanberraAIM556::dev_state");
  }

  if( state ) {
    argout = Tango::ON;
    set_status("Acquisition is running");
  } else {
    argout = Tango::OFF;
    set_status("Acquisition is stopped");
  }

  attr_LiveTime_read[0] = (double)live / 100.0;
  attr_RealTime_read[0] = (double)real / 100.0;

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::dev_state
	set_state(argout);    // Give the state to Tango.
	if (argout!=Tango::ALARM)
		Tango::DeviceImpl::dev_state();
	return get_state();  // Return it after Tango management.
}
//--------------------------------------------------------
/**
 *	Command On related method
 *	Description: Start the aquisition
 *
 */
//--------------------------------------------------------
void CanberraAIM556::on()
{
	DEBUG_STREAM << "CanberraAIM556::On()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::on) ENABLED START -----*/

	if(get_state()==Tango::ON) {
	  // Do nothing if already on
	  return;
	}

  // Start acquisition
  if( !aim->SetAcquisitionState(attr_ADC_read[0],1) ) {
    Tango::Except::throw_exception(
            (const char *)"CanberraAIM556::error_write",
            (const char *)aim->GetLastError().c_str(),
            (const char *)"CanberraAIM556::on() SetAcquisitionState");
  }

  /*----- PROTECTED REGION END -----*/	//	CanberraAIM556::on
}
//--------------------------------------------------------
/**
 *	Command Off related method
 *	Description: Stop the acquistion
 *
 */
//--------------------------------------------------------
void CanberraAIM556::off()
{
	DEBUG_STREAM << "CanberraAIM556::Off()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::off) ENABLED START -----*/

  // Stop acquisition
  if( !aim->SetAcquisitionState(attr_ADC_read[0],0) ) {
    Tango::Except::throw_exception(
            (const char *)"CanberraAIM556::error_write",
            (const char *)aim->GetLastError().c_str(),
            (const char *)"CanberraAIM556::off() SetAcquisitionState");
  }

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::off
}
//--------------------------------------------------------
/**
 *	Command Reset related method
 *	Description: Clear memory
 *
 */
//--------------------------------------------------------
void CanberraAIM556::reset()
{
	DEBUG_STREAM << "CanberraAIM556::Reset()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::reset) ENABLED START -----*/

  // Reset AIM
  if( !aim->Clear(attr_ADC_read[0],attr_Channels_read[0]*4) ) {
    Tango::Except::throw_exception(
            (const char *)"CanberraAIM556::error_write",
            (const char *)aim->GetLastError().c_str(),
            (const char *)"CanberraAIM556::reset");
  }

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::reset
}
//--------------------------------------------------------
/**
 *	Command ClearMemory related method
 *	Description: Reset the acquisition memory to 0
 *
 */
//--------------------------------------------------------
void CanberraAIM556::clear_memory()
{
	DEBUG_STREAM << "CanberraAIM556::ClearMemory()  - " << device_name << endl;
	/*----- PROTECTED REGION ID(CanberraAIM556::clear_memory) ENABLED START -----*/

	bool isON = (get_state() == Tango::ON);

  if( isON ) {
    off();
    dev_state();
  }

  // Clear AIM memory
  if( !aim->ClearMem(attr_Channels_read[0]*4) ) {
    Tango::Except::throw_exception(
            (const char *)"CanberraAIM556::error_write",
            (const char *)aim->GetLastError().c_str(),
            (const char *)"CanberraAIM556::clear_memory() ClearMem");
  }

  reset();

  if( isON ) {
    on();
    dev_state();
  }

	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::clear_memory
}
//--------------------------------------------------------
/**
 *	Method      : CanberraAIM556::add_dynamic_commands()
 *	Description : Create the dynamic commands if any
 *                for specified device.
 */
//--------------------------------------------------------
void CanberraAIM556::add_dynamic_commands()
{
	/*----- PROTECTED REGION ID(CanberraAIM556::add_dynamic_commands) ENABLED START -----*/
	
	//	Add your own code to create and add dynamic commands if any
	
	/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::add_dynamic_commands
}

/*----- PROTECTED REGION ID(CanberraAIM556::namespace_ending) ENABLED START -----*/

//	Additional Methods
void CanberraAIM556::SetupMCA() {

  // Setup acquisition
  int mode;

  switch(attr_Mode_read[0]) {

    case _ModeEnum::_PHA:
      mode = NCP_C_AMODE_PHA;
      break;
    case _ModeEnum::_MCS:
      mode = NCP_C_AMODE_MCS;
      break;
    case _ModeEnum::_LIST:
      mode = NCP_C_AMODE_DLIST;
      break;
    default:
      Tango::Except::throw_exception(
              (const char *)"CanberraAIM556::error_write",
              (const char *)"Unexpected mode vale",
              (const char *)"CanberraAIM556::on() SetupAcquisition");

  }

  if( !aim->SetupAcquisition(attr_ADC_read[0],
                             attr_Channels_read[0],
                             (int)(attr_PresetLiveTime_read[0]*100.0),
                             (int)(attr_PresetRealTime_read[0]*100.0),
                             mode)) {
    Tango::Except::throw_exception(
            (const char *)"CanberraAIM556::error_write",
            (const char *)aim->GetLastError().c_str(),
            (const char *)"CanberraAIM556::on() SetupAcquisition");
  }

}

bool CanberraAIM556::InitSetPoint(string attName,double *value) {

  bool found = false;
  Tango::DbData db_data;
  db_data.push_back(Tango::DbDatum(attName));
  get_db_device()->get_attribute_property(db_data);

  for (int i=0;i < (int)db_data.size();i++)
  {
    long nb_prop;
    double a;
    //cout << "db_data.name=" << db_data[i].name << endl;
    db_data[i] >> nb_prop;
    i++;
    for(int k=0;k < nb_prop;k++)
    {
      if( strcasecmp(db_data[i].name.c_str(),"__value")==0 ) {
        db_data[i] >> *value;
        found = true;
      }
      i++;
    }
  }

  return found;

}

/*----- PROTECTED REGION END -----*/	//	CanberraAIM556::namespace_ending
} //	namespace
